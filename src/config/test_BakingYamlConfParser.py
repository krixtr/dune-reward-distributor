from unittest import TestCase

from cli.wallet_client_manager import WalletClientManager
from config.addr_type import AddrType
from config.yaml_baking_conf_parser import BakingYamlConfParser
from rpc.rpc_block_api import RpcBlockApiImpl
from Constants import PUBLIC_NODE_URL

network={'NAME': 'MAINNET'}
mainnet_public_node_url = "https://mainnet-node.dunscan.io"

class TestYamlAppConfParser(TestCase):
    def test_validate(self):

        data_fine = """
        version : 1.0
        baking_address : dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF
        payment_address : dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF
        founders_map : {'KT2Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj':0.5,'KT3Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj':0.5}
        owners_map : {'KT2Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj':0.5,'KT3Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj':0.5}
        service_fee : 4.53
        reactivate_zeroed: False
        delegator_pays_ra_fee: True
        """

        managers = {'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF': 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF',
                    'KT1Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj': 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF'}
        contr_dict_by_alias = {}
        addr_dict_by_pkh = {
            "dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF": {"pkh": "dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF", "originated": False,
                                                     "alias": "main1", "sk": True, "revealed" : True,
                                                     "manager": "dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF"},
            "KT1Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj": {"pkh": "KT1Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj", "originated": True,
                                                     "alias": "kt1", "sk": True, "revealed" : True,
                                                     "manager": "dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF"}
        }

        wallet_client_manager = WalletClientManager(client_path=None, node_addr=None, addr_dict_by_pkh=addr_dict_by_pkh, contr_dict_by_alias=contr_dict_by_alias, managers=managers)

        block_api = RpcBlockApiImpl(network, PUBLIC_NODE_URL["MAINNET"][0])
        cnf_prsr = BakingYamlConfParser(data_fine, wallet_client_manager, provider_factory=None, network_config=network,node_url=mainnet_public_node_url,block_api=block_api)


        cnf_prsr.parse()
        cnf_prsr.validate()

        self.assertEqual(cnf_prsr.get_conf_obj_attr('baking_address'), 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF')
        self.assertEqual(cnf_prsr.get_conf_obj_attr('payment_address'), 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF')
        self.assertEqual(cnf_prsr.get_conf_obj_attr('__payment_address_pkh'), 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF')
        self.assertEqual(cnf_prsr.get_conf_obj_attr('__payment_address_manager'), 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF')
        self.assertEqual(cnf_prsr.get_conf_obj_attr('__payment_address_type'), AddrType.DN)

        self.assertEqual(cnf_prsr.get_conf_obj_attr('min_delegation_amt'), 0)

        self.assertEqual(cnf_prsr.get_conf_obj_attr('reactivate_zeroed'), False)
        self.assertEqual(cnf_prsr.get_conf_obj_attr('delegator_pays_ra_fee'), True)

    def test_validate_no_founders_map(self):
        data_no_founders = """
        version : 1.0
        baking_address : dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF
        payment_address : dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF
        owners_map : {'KT2Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj':0.5,'KT3Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj':0.5}
        service_fee : 4.5
        reactivate_zeroed: False
        delegator_pays_ra_fee: True
        """

        managers_map = {'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF': 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF'}


        contr_dict_by_alias = {}
        addr_dict_by_pkh = {
            "dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF": {"pkh": "dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF", "originated": False,
                                                     "alias": "main1", "sk": True,
                                                     "manager": "dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF"}}

        wallet_client_manager = WalletClientManager(client_path=None, node_addr=None, addr_dict_by_pkh=addr_dict_by_pkh,
                                                    contr_dict_by_alias=contr_dict_by_alias, managers=managers_map)

        block_api = RpcBlockApiImpl(network, PUBLIC_NODE_URL["MAINNET"][0])
        cnf_prsr = BakingYamlConfParser(data_no_founders, wallet_client_manager, provider_factory=None, network_config=network,
                                        node_url=mainnet_public_node_url, block_api=block_api)

        cnf_prsr.parse()
        cnf_prsr.validate()

        self.assertEqual(cnf_prsr.get_conf_obj_attr('baking_address'), 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF')
        self.assertEqual(cnf_prsr.get_conf_obj_attr('payment_address'), 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF')
        self.assertEqual(cnf_prsr.get_conf_obj_attr('__payment_address_pkh'), 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF')
        self.assertEqual(cnf_prsr.get_conf_obj_attr('__payment_address_manager'), 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF')
        self.assertEqual(cnf_prsr.get_conf_obj_attr('__payment_address_type'), AddrType.DN)

        self.assertEqual(cnf_prsr.get_conf_obj_attr('founders_map'), dict())
        self.assertEqual(cnf_prsr.get_conf_obj_attr('specials_map'), dict())
        self.assertEqual(cnf_prsr.get_conf_obj_attr('supporters_set'), set())

        self.assertEqual(cnf_prsr.get_conf_obj_attr('min_delegation_amt'), 0)

        self.assertEqual(cnf_prsr.get_conf_obj_attr('reactivate_zeroed'), False)
        self.assertEqual(cnf_prsr.get_conf_obj_attr('delegator_pays_ra_fee'), True)

    def test_validate_pymnt_alias(self):
        data_no_founders = """
        version : 1.0
        baking_address : dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF
        payment_address : dnPay
        owners_map : {'KT2Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj':0.5,'KT3Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj':0.5}
        service_fee : 4.5
        min_delegation_amt : 100
        reactivate_zeroed: False
        delegator_pays_ra_fee: True
        """

        managers_map = {'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF': 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF',
                        'KT1Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj': 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF'}

        contr_dict_by_alias = {'kt': 'KT1Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj', 'dnPay': 'dn1Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj'}
        addr_dict_by_pkh = {
            "dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF": {"pkh": "dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF", "originated": False,
                                                     "alias": "dn1", "sk": True,
                                                     "manager": "dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF"},
            "dn1Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj": {"pkh": "dn1Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj", "originated": False,
                                                     "alias": "dnPay", "sk": True, "revealed":True,
                                                     "manager": "dn1Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj"}
        }

        wallet_client_manager = WalletClientManager(client_path=None, node_addr=None, addr_dict_by_pkh=addr_dict_by_pkh, contr_dict_by_alias=contr_dict_by_alias, managers=managers_map)

        block_api = RpcBlockApiImpl(network, PUBLIC_NODE_URL["MAINNET"][0])
        cnf_prsr = BakingYamlConfParser(data_no_founders, wallet_client_manager, provider_factory=None, network_config=network, node_url=mainnet_public_node_url, block_api=block_api)

        cnf_prsr.parse()
        cnf_prsr.validate()

        self.assertEqual(cnf_prsr.get_conf_obj_attr('baking_address'), 'dn1Pz6KLQNcYvmK59M3gtMjUjzE6fprWFwrF')
        self.assertEqual(cnf_prsr.get_conf_obj_attr('payment_address'), 'dnPay')
        self.assertEqual(cnf_prsr.get_conf_obj_attr('__payment_address_pkh'), 'dn1Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj')
        self.assertEqual(cnf_prsr.get_conf_obj_attr('__payment_address_manager'), 'dn1Z1tMai15JWUWeN2PKL9faXXVPMuWamzJj')
        self.assertEqual(cnf_prsr.get_conf_obj_attr('__payment_address_type'), AddrType.DNALS)

        self.assertEqual(cnf_prsr.get_conf_obj_attr('founders_map'), dict())
        self.assertEqual(cnf_prsr.get_conf_obj_attr('specials_map'), dict())
        self.assertEqual(cnf_prsr.get_conf_obj_attr('supporters_set'), set())

        self.assertEqual(cnf_prsr.get_conf_obj_attr('min_delegation_amt'), 100)

        self.assertEqual(cnf_prsr.get_conf_obj_attr('reactivate_zeroed'), False)
        self.assertEqual(cnf_prsr.get_conf_obj_attr('delegator_pays_ra_fee'), True)
