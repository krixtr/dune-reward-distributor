import requests

from api.block_api import BlockApi
from exception.dunscan import DunScanException
from log_config import main_logger

logger = main_logger

HEAD_API = {'MAINNET': {'HEAD_API_URL': 'https://api.dunscan.io/v3/head'},
            'TESTNET': {'HEAD_API_URL': 'http://api.testnet.dunscan.io/v3/head'}
            }

REVELATION_API = {'MAINNET': {'HEAD_API_URL': 'https://api.dunscan.io/v3/operations/%PKH%?type=Reveal'},
                  'TESTNET': {'HEAD_API_URL': 'https://api.testnet.dunscan.io/v3/operations/%PKH%?type=Reveal'}
                  }


class DunScanBlockApiImpl(BlockApi):

    def __init__(self, nw):
        super(DunScanBlockApiImpl, self).__init__(nw)

        self.head_api = HEAD_API[nw['NAME']]
        if self.head_api is None:
            raise Exception("Unknown network {}".format(nw))

        self.revelation_api = REVELATION_API[nw['NAME']]

    def get_current_level(self, verbose=False):
        uri = self.head_api['HEAD_API_URL']

        if verbose:
            logger.debug("Requesting {}".format(uri))

        resp = requests.get(uri, timeout=5)
        if resp.status_code != 200:
            # This means something went wrong.
            raise DunScanException('GET {} {}'.format(uri, resp.status_code))
        root = resp.json()

        if verbose:
            logger.debug("Response from dunscan is: {}".format(root))

        current_level = int(root["level"])

        if verbose:
            logger.debug("Current level from dunscan is: {}".format(current_level))

        return current_level

    def get_current_cycle(self, verbose=False):
        uri = self.head_api['HEAD_API_URL']

        if verbose:
            logger.debug("Requesting {}".format(uri))

        resp = requests.get(uri, timeout=5)
        if resp.status_code != 200:
            # This means something went wrong.
            raise DunScanException('GET {} {}'.format(uri, resp.status_code))
        root = resp.json()

        if verbose:
            logger.debug("Response from dunscan is: {}".format(root))

        cycle = int(root["cycle"])

        if verbose:
            logger.debug("Current cycle from dunscan is: {}".format(cycle))

        return cycle

    def get_revelation(self, pkh, verbose=False):
        uri = self.revelation_api['HEAD_API_URL'].replace("%PKH%", pkh)

        if verbose:
            logger.debug("Requesting {}".format(uri))

        resp = requests.get(uri, timeout=5)
        if resp.status_code != 200:
            # This means something went wrong.
            raise DunScanException('GET {} {}'.format(uri, resp.status_code))
        root = resp.json()

        if verbose:
            logger.debug("Response from dunscan is: {}".format(root))

        return len(root) > 0
