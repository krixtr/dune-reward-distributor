from api.reward_api import RewardApi
from dunscan.dunscan_reward_provider_helper import DunScanRewardProviderHelper

from log_config import main_logger
from model.reward_provider_model import RewardProviderModel

logger = main_logger


class DunscanRewardApiImpl(RewardApi):

    def __init__(self, nw, baking_address, verbose=False):
        super().__init__()

        self.verbose = verbose
        self.logger = main_logger
        self.helper = DunScanRewardProviderHelper(nw, baking_address)

    def get_rewards_for_cycle_map(self, cycle):
        root = self.helper.get_rewards_for_cycle(cycle, self.verbose)

        delegate_staking_balance = root["delegate_staking_balance"]
        blocks_rewards = root["blocks_rewards"]
        future_blocks_rewards = root["future_blocks_rewards"]
        endorsements_rewards = root["endorsements_rewards"]
        future_endorsements_rewards = root["future_endorsements_rewards"]
        lost_rewards_denounciation = root["lost_rewards_denounciation_baking"] + root["lost_rewards_denounciation_endorsement"]
        lost_fees_denounciation = root["lost_fees_denounciation_baking"] + root["lost_fees_denounciation_endorsement"]
        fees = root["fees"]

        total_reward_amount = (blocks_rewards + endorsements_rewards + future_blocks_rewards +
                               future_endorsements_rewards + fees - lost_rewards_denounciation - lost_fees_denounciation)

        delegators_balances_dict = root["delegators_balances"]

        return RewardProviderModel(delegate_staking_balance, total_reward_amount, delegators_balances_dict)
