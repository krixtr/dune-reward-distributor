import requests

from time import sleep
from exception.dunscan import DunScanException
from log_config import main_logger

MAX_PER_PAGE = 50

logger = main_logger

nb_delegators_call = 'nb_delegators/{}?cycle={}'
rewards_split_call = 'rewards_split/{}?cycle={}&p={}&number={}'
delegator_balance_call = 'balance/{}'

API = {'MAINNET': {'API_URL': 'http://api.dunscan.io/v3/'},
       'TESTNET': {'API_URL': 'http://api.testnet.dunscan.io/v3/'},
       'DEVNET': {'API_URL': 'http://api.devnet.dunscan.io/v3/'}
       }


class DunScanRewardProviderHelper:

    def __init__(self, nw, baking_address):
        super(DunScanRewardProviderHelper, self).__init__()

        self.api = API[nw['NAME']]
        if self.api is None:
            raise DunScanException("Unknown network {}".format(nw))

        self.baking_address = baking_address

    def __get_nb_delegators(self, cycle, verbose=False):
        uri = self.api['API_URL'] + nb_delegators_call.format(self.baking_address, cycle)

        if verbose:
            logger.debug("Requesting {}".format(uri))

        resp = requests.get(uri, timeout=5)
        if resp.status_code != 200:
            # This means something went wrong.
            raise DunScanException('GET {} {}'.format(uri, resp.status_code))
        root = resp.json()

        if verbose:
            logger.debug("Response from dunscan is {}".format(resp.content.decode("utf8")))

        return root

    def get_rewards_for_cycle(self, cycle, verbose=False):

        nb_delegators = self.__get_nb_delegators(cycle, verbose)[0]
        nb_delegators_remaining = nb_delegators

        page = 0
        root = {"delegate_staking_balance": 0, "delegators_nb": 0, "delegators_balances": {}, "blocks_rewards": 0,
                "endorsements_rewards": 0, "fees": 0, "future_blocks_rewards": 0, "future_endorsements_rewards": 0,
                "gain_from_denounciation_baking": 0, "lost_deposit_from_denounciation_baking": 0,
                "gain_from_denounciation_endorsement": 0, "lost_deposit_from_denounciation_endorsement": 0,
                "lost_rewards_denounciation_endorsement": 0,"lost_rewards_denounciation_baking": 0,
                "lost_fees_denounciation_endorsement": 0, "lost_fees_denounciation_baking": 0,
                "revelation_rewards": 0, "lost_revelation_rewards": 0, "lost_revelation_fees": 0}

        while nb_delegators_remaining > 0:

            uri = self.api['API_URL'] + rewards_split_call.format(self.baking_address, cycle, page, MAX_PER_PAGE)

            if verbose:
                logger.debug("Requesting {}".format(uri))

            resp = requests.get(uri, timeout=5)

            if verbose:
                logger.debug("Response from dunscan is {}".format(resp.content.decode("utf8")))

            if resp.status_code != 200:
                # This means something went wrong.
                raise DunScanException('GET {} {}'.format(uri, resp.status_code))

            json_resp = resp.json()

            if page == 0:  # keep first result as basis; append 'delegators_balance' from other responses

                root["delegate_staking_balance"] = int(json_resp["delegate_staking_balance"])
                root["delegators_nb"] = int(json_resp["delegators_nb"])
                root["blocks_rewards"] = int(json_resp["blocks_rewards"])
                root["endorsements_rewards"] = int(json_resp["endorsements_rewards"])
                root["fees"] = int(json_resp["fees"])

                root["future_blocks_rewards"] = int(json_resp["future_blocks_rewards"])
                root["future_endorsements_rewards"] = int(json_resp["future_endorsements_rewards"])

                root["gain_from_denounciation_baking"] = int(json_resp["gain_from_denounciation_baking"])
                root["lost_deposit_from_denounciation_baking"] = int(json_resp["lost_deposit_from_denounciation_baking"])
                root["lost_rewards_denounciation_baking"] = int(json_resp["lost_rewards_denounciation_baking"])
                root["lost_fees_denounciation_baking"] = int(json_resp["lost_fees_denounciation_baking"])

                root["gain_from_denounciation_endorsement"] = int(json_resp["gain_from_denounciation_endorsement"])
                root["lost_deposit_from_denounciation_endorsement"] = int(json_resp["lost_deposit_from_denounciation_endorsement"])
                root["lost_rewards_denounciation_endorsement"] = int(json_resp["lost_rewards_denounciation_endorsement"])
                root["lost_fees_denounciation_endorsement"] = int(json_resp["lost_fees_denounciation_endorsement"])

                root["revelation_rewards"] = int(json_resp["revelation_rewards"])
                root["lost_revelation_rewards"] = int(json_resp["lost_revelation_rewards"])
                root["lost_revelation_fees"] = int(json_resp["lost_revelation_fees"])
                
            # "convert" 'delegators_balance' from dunscan to our new object
            for d in json_resp["delegators_balance"]:
                balances = {"staking_balance": int(d["balance"]), "current_balance": 0}
                delegator_address = d["account"]["tz"]
                root["delegators_balances"][delegator_address] = balances

            nb_delegators_listed = len(json_resp["delegators_balance"])
            nb_delegators_remaining = nb_delegators_remaining - nb_delegators_listed
            page = page + 1

        #
        # Now, need to get current balance of each delegator to determine if 0 balance
        #
        staked_bal_delegators = root["delegators_balances"].keys()
        curr_bal_delegators = []

        for d in staked_bal_delegators:

            uri = self.api['API_URL'] + delegator_balance_call.format(d)

            if verbose:
                logger.debug("Requesting current balance {}".format(uri))

            sleep(0.5) # be nice to dunscan

            resp = requests.get(uri, timeout=5)

            if verbose:
                logger.debug("Response from dunscan is {}".format(resp.content.decode("utf8")))

            if resp.status_code != 200:
                # This means something went wrong.
                raise DunScanException('GET {} {}'.format(uri, resp.status_code))

            # Save current balance to global object
            root["delegators_balances"][d]["current_balance"] = int(resp.json()[0])
            curr_bal_delegators.append(d)

        # Sanity check
        n_curr_balance = len(curr_bal_delegators)
        n_stake_balance = len(staked_bal_delegators)

        if n_curr_balance != n_stake_balance:
            raise DunScanException('Did not fetch all balances {}/{}'.format(n_curr_balance, n_stake_balance))

        return root
