<img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/16615422/drd_logo.png" width="140px" height="80px" />

## Dune Reward Distributor

Dune Reward Distributor is a clone of the Tezos Reward Distributor (https://github.com/habanoz/tezos-reward-distributor), for the https://dune.network/.

DRD is software for distributing baking rewards with delegators. This is a full scale application which can run in the background all the time. It can track cycles and make payments. 

DRD supports complex payments rules, pays in batches, provides two back ends for calculations (rpc and dunscan), and much more.

### Support

DRD community support can be found by joining our discord chat room: https://discord.gg/Dx2PF2s

DO NOT open gitlab issues requesting support; these will be closed without comment. Join the discord chat room.

### Requirements and Setup:

Python 3 is required. You can use following commands to install.

```
sudo apt-get update
sudo apt-get -y install python3-pip
```

Download the application repository using git clone:

```
git clone https://gitlab.com/krixtr/dune-reward-distributor.git
```

To install required modules, use pip with requirements.txt provided.

```
cd dune-reward-distributor
pip3 install -r requirements.txt
```

Regulary check and upgrade to the latest available version:

```
git pull
```

### How to Run:

For a list of parameters, run:

```
python3 src/main.py --help
```

The most common use case is to run on mainnet and start to make payments from last released cycle or continue making payments from the previous cycle.

```
python3 src/main.py
```

For more example commands, please consult the documentation

https://gitlab.com/krixtr/dune-reward-distributor/tree/master/docs

### Baker Configuration:

Each baker has its own configuration and policy. A payment system should be flexible enough to cover needs of bakers. The application uses a yaml file for loading baker specific configurations.

Configuration tool can be used to create baking configuration file interactively. Also an example configuration file is present under examples directory. 

DRD can work as a linux service. It expects use of `dune-signer` for encrypted payment accounts. Unencrypted payment accounts can be used without `dune-signer`, but this is insecure and not recommended. If a payment account is encrypted and not configured to be signed by `dune-signer`, DRD will freeze. 

### Linux Service

It is possible to add dune-reward-distributer as a Linux service so that it can run in the background.

If docker is used, make sure user is in docker group
```
sudo usermod -a -G docker $USER
```

In order to set up the service with default configuration arguments, run the following command:

```
sudo python3 service_add.py
```

### Email Setup

Get emails for payment reports at each cycle. Fill email.ini file with your email details to receive payment emails.

### Fee Setup

fee.ini file contains details about transaction fees. Currently the fee value specified under DEFAULT domain is used as fee amount. It is in mutez.
