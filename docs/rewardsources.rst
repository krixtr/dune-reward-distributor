Reward Sources
==============

There are two sources for retreiving the payout data that DRD needs for calculating payouts: dunscan.io, or local RPC.

dunscan.io
----------

You can view the payout information manually by visiting the 'Rewards' tab of the delegate's address page on dunscan.io.

All of the payout information is already calculated by dunscan.io. DRD performs a few simple fetches from the dunscan.io API and creates the payout transactions using this data.

This is the simplest method and the default.

Local RPC
---------

This method uses Dune RPC calls to your local node to fetch all payout information needed. No external sources are required.

Archive Mode
~~~~~~~~~~~~
The default operation mode for dune-node is "full" which garbage collects the data needed for payouts older than 5 cycles.

In order to fetch the information needed for payouts, DRD needs to look 6 cycles back.
The 6th cycle is the first cycle with unfrozen rewards, which are the payable rewards.
Because a full node cleans up data older than 5 cycles, the data DRD needs simply does not exist in a full node.

If you wish to use the 'Local RPC' method, you must be running a dune node in "archive" mode.
Consult the dune documentation for re-configuring your node to operate in this mode.
