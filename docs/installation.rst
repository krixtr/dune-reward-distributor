## How to get and install Dune Reward Distributor

### Requirements and Setup:

Python 3 is required. You can use following commands to install.

```
sudo apt-get update
sudo apt-get -y install python3-pip
```

Download the application repository using git clone:

```
git clone https://gitlab.com/krixtr/dune-reward-distributor.git
```

To install required modules, use pip with requirements.txt provided.

```
cd dune-reward-distributor
pip3 install -r requirements.txt
```

Regulary check and upgrade to the latest available version:

```
git pull
```
