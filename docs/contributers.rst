For developers
=====================================================

If you are interested in improving our project, this is just great! Our community looks forward to your contribution. Please follow this process:

1. Start a bug/feature issue on gitlab about your proposed change. Continue discussion until a solution is decided.
2. Fork the repo on your gitlab account. Create a new branch on your fork for your contribution to make it easier to merge your changes back.
3. Make your changes to your fork/branch. Be sure that your code passes existing unit tests. Please add unit tests for your work if appropriate. It usually is.
4. Push your changes to your fork/branch in gitlab. Don't push it to your master branch! If you do it will make it harder to submit new changes later.
5. Submit a merge request to the repo from your commit page on gitlab.


Principles on the application evolution:

1. Discuss the solution with repository owners and other community members. Start the development after everybody agrees on a solution. 
2. Prefer the simpler solution. Try to make as fewer changes as possible. 
3. Update documentation wherever it is meaningful.
4. Create and run unit tests for the changes. Please do not sends PRs for untested changes. Payment domain requires the utmost responsibility.
5. Follow naming conventions.
6. Avoid code repetition. Make use of Object Oriented design principles when possible. 
7. More configuration parameters do not always mean more flexibility. We do not want the application to turn into configuration hell which nobody can use.
8. The idea of community members is very important because this repo is just a sequence of statements if nobody uses and benefit.
9. A change will be accepted if it will benefit the community. Particular requests which only very few people will take advantage will not be accepted to leave the code as simple as possible. 
